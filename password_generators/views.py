from django.http import HttpResponse
from password_generators.services import generate_password

def index(request):
    password = generate_password()
    return HttpResponse(f'Your password is {password}.')

